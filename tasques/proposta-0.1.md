<!--
SPDX-FileCopyrightText: 2023 Xavier Bordoy

SPDX-License-Identifier: CC-BY-SA-4.0
-->

---
title: "Exercicis automàtics: proposta per a la versió 0.1"
date: "4 de novembre de 2022"
abstract: "Proposta de com vull que sigui la versió 0.1"
---

# Objectiu

Permet generar fulls d'exercicis i exàmens a partir d'una base de dades d'exercicis

# Pantalla 1

> Quin tipus d'exercicis voleu generar?
>
> - [ ] equacions de primer grau
> - [ ] equacions de segon grau
> - [ ] regles de tres

L'usuari tria la primera

# Pantalla 2

> Quin tipus d'equacions de primer grau voleu generar?
>
> - [ ] $ax + bx = cx + d$ on $a \in \{1, \ldots, 10\}$, $b \in \{-10, \ldots, 10\}$ i $c \in \{-20, \ldots, 20\}$
> - [ ] $\frac{ax}{b} + cx = \frac{dx}{3}$, on $a$...

L'usuari pot triar les fites superiors i inferiors dels conjunts corresponents als paràmetres mitjançant una interfície web

# Pantalla 3

> Com voleu generar els exercicis?
>
> - [ ] $N$ exercicis de cada tipus, separats
> - [ ] $N$ exercicis mesclats aleatòriament

(L'usuari pot triar la $N$ a posteriori)

> i quin tipus de resposta voleu que s'introdueixi?
>
> - [ ] tipus test
> - [ ] emparallament
> - [ ] resposta oberta

# Pantalla 4

> Quin document voleu generar?

- [ ] pdf
- [ ] test online

# Implementació

- Es podria fer en crystal o amb rust de manera que només tendríem un binari que no necessitaria instal·lar dependències com a paquets al sistema. Amb crystal es podria aprofitar la feina feta de les plantilles ERB amb ruby.

- Necessitaria una interfície web.
