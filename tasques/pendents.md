# Versions 


## 0.0.5 

### Notes 

- [#14] Versió de revisió i adobar errors

### Tasks 

- [#5] Revisar els exercicis que ja tenc: tots han de tenir una pàgina d'exercicis, i les solucions i resolucions. Les resolucions han de ser com si fossin manuscrites 
- [#9] posar la solució i resolució de les equacions de segon gran de tres maneres diferents (a pàgines a part): per fórmula general de segon grau, completant quadrats i per canvi de variable [https://scholar.social/@somenxavier/105955366581927908]. D'aquesta manera, sempre podré fer servir els fitxers independentment de com expliqui la resolució 
- [#15] espa 1: posar exercicis de simplificació de fraccions 
- [#19] espa 1: posar la resolució als problemes de proporcionalitat 
- [#26] espa 1: incorporar els exercicis de nombres sencers del primer quadrimestre del classroom 
- [#29] espa 3: incorporar els exercicis de diagrames de sectors i barres 
- [#30] espa 1: incorporar els exercicis de càlcul d'àrea i perímetre de figures plantes amb triangularització 
- [#31] espa 1: incorporar els exercicis de volum de cossos geomètrics 
- [#32] espa 1: incorporar els exercicis d'àrea i perímetre de cercles i corones circulars 
- [#33] espa 1: posar exercicis de conversió d'unitats de temps 
- [#41]  Generar 6 versions dels deures. D'aquesta manera no els he de generar quan els necessiti, sinó que ja els tenc generats. Així la gent que ve més tard, ja puc assignar-los la tasca al mateix moment. Tenir còpia impressa. 
- [#43] Passar tots els deures que tenia al classroom de ESPA 1 i ESPA 3 
- [#48] Les equacions de primer grau amb denominadors tenen la solució i la resolució malament 

# Versions 


## 0.0.5 

### Notes 

- [#14] Versió de revisió i adobar errors

### Tasks 

- [#5] Revisar els exercicis que ja tenc: tots han de tenir una pàgina d'exercicis, i les solucions i resolucions. Les resolucions han de ser com si fossin manuscrites 
- [#9] posar la solució i resolució de les equacions de segon gran de tres maneres diferents (a pàgines a part): per fórmula general de segon grau, completant quadrats i per canvi de variable [https://scholar.social/@somenxavier/105955366581927908]. D'aquesta manera, sempre podré fer servir els fitxers independentment de com expliqui la resolució 
- [#15] espa 1: posar exercicis de simplificació de fraccions 
- [#19] espa 1: posar la resolució als problemes de proporcionalitat 
- [#26] espa 1: incorporar els exercicis de nombres sencers del primer quadrimestre del classroom 
- [#29] espa 3: incorporar els exercicis de diagrames de sectors i barres 
- [#30] espa 1: incorporar els exercicis de càlcul d'àrea i perímetre de figures plantes amb triangularització 
- [#31] espa 1: incorporar els exercicis de volum de cossos geomètrics 
- [#32] espa 1: incorporar els exercicis d'àrea i perímetre de cercles i corones circulars 
- [#33] espa 1: posar exercicis de conversió d'unitats de temps 
- [#41]  Generar 6 versions dels deures. D'aquesta manera no els he de generar quan els necessiti, sinó que ja els tenc generats. Així la gent que ve més tard, ja puc assignar-los la tasca al mateix moment. Tenir còpia impressa. 
- [#43] Passar tots els deures que tenia al classroom de ESPA 1 i ESPA 3 
- [#48] Les equacions de primer grau amb denominadors tenen la solució i la resolució malament 

## 0.0.6 

### Notes 

- [#28] Noves millores

### Tasks 

- [#16] Posar 'make nom-carpeta N' per a generar N fitxers de tots els exercicis a la carpeta 'nom-carpeta' 
- [#40] Posar 'make <nivell>' de manera que es generin tots els pdf d'un mateix nivell 

# Versions 


## 0.0.5 

### Notes 

- [#14] Versió de revisió i adobar errors

### Tasks 

- [#5] Revisar els exercicis que ja tenc: tots han de tenir una pàgina d'exercicis, i les solucions i resolucions. Les resolucions han de ser com si fossin manuscrites 
- [#9] posar la solució i resolució de les equacions de segon gran de tres maneres diferents (a pàgines a part): per fórmula general de segon grau, completant quadrats i per canvi de variable [https://scholar.social/@somenxavier/105955366581927908]. D'aquesta manera, sempre podré fer servir els fitxers independentment de com expliqui la resolució 
- [#15] espa 1: posar exercicis de simplificació de fraccions 
- [#19] espa 1: posar la resolució als problemes de proporcionalitat 
- [#26] espa 1: incorporar els exercicis de nombres sencers del primer quadrimestre del classroom 
- [#29] espa 3: incorporar els exercicis de diagrames de sectors i barres 
- [#30] espa 1: incorporar els exercicis de càlcul d'àrea i perímetre de figures plantes amb triangularització 
- [#31] espa 1: incorporar els exercicis de volum de cossos geomètrics 
- [#32] espa 1: incorporar els exercicis d'àrea i perímetre de cercles i corones circulars 
- [#33] espa 1: posar exercicis de conversió d'unitats de temps 
- [#41]  Generar 6 versions dels deures. D'aquesta manera no els he de generar quan els necessiti, sinó que ja els tenc generats. Així la gent que ve més tard, ja puc assignar-los la tasca al mateix moment. Tenir còpia impressa. 
- [#43] Passar tots els deures que tenia al classroom de ESPA 1 i ESPA 3 
- [#48] Les equacions de primer grau amb denominadors tenen la solució i la resolució malament 

## 0.0.6 

### Notes 

- [#28] Noves millores

### Tasks 

- [#16] Posar 'make nom-carpeta N' per a generar N fitxers de tots els exercicis a la carpeta 'nom-carpeta' 
- [#40] Posar 'make <nivell>' de manera que es generin tots els pdf d'un mateix nivell 

## 0.0.7 

### Notes 

- [#11] Tenir cobert tot el currículum

### Tasks 

- [#10] Mirar el currículum per determinar els tipus d'exercicis automàtics que me falten d'ESPA (tots els nivells) i posar tasques corresponents 
- [#36] Fer una versió web per a què els alumnes s'entrenassin: un alumne podria escollir un exercici i fer-lo. Hauria de ser més modular: definir primer les variables, mostrar a part l'enunciat, les solucions i la resolució 
- [#37] Fer una versió web per a què els alumnes: dur comptabilitat (tipus classroom o deltamath) de qui fa què i una espècie de puntuació 

# Versions 


## 0.0.5 

### Notes 

- [#14] Versió de revisió i adobar errors

### Tasks 

- [#5] Revisar els exercicis que ja tenc: tots han de tenir una pàgina d'exercicis, i les solucions i resolucions. Les resolucions han de ser com si fossin manuscrites 
- [#9] posar la solució i resolució de les equacions de segon gran de tres maneres diferents (a pàgines a part): per fórmula general de segon grau, completant quadrats i per canvi de variable [https://scholar.social/@somenxavier/105955366581927908]. D'aquesta manera, sempre podré fer servir els fitxers independentment de com expliqui la resolució 
- [#15] espa 1: posar exercicis de simplificació de fraccions 
- [#19] espa 1: posar la resolució als problemes de proporcionalitat 
- [#26] espa 1: incorporar els exercicis de nombres sencers del primer quadrimestre del classroom 
- [#29] espa 3: incorporar els exercicis de diagrames de sectors i barres 
- [#30] espa 1: incorporar els exercicis de càlcul d'àrea i perímetre de figures plantes amb triangularització 
- [#31] espa 1: incorporar els exercicis de volum de cossos geomètrics 
- [#32] espa 1: incorporar els exercicis d'àrea i perímetre de cercles i corones circulars 
- [#33] espa 1: posar exercicis de conversió d'unitats de temps 
- [#41]  Generar 6 versions dels deures. D'aquesta manera no els he de generar quan els necessiti, sinó que ja els tenc generats. Així la gent que ve més tard, ja puc assignar-los la tasca al mateix moment. Tenir còpia impressa. 
- [#43] Passar tots els deures que tenia al classroom de ESPA 1 i ESPA 3 
- [#48] Les equacions de primer grau amb denominadors tenen la solució i la resolució malament 

## 0.0.6 

### Notes 

- [#28] Noves millores

### Tasks 

- [#16] Posar 'make nom-carpeta N' per a generar N fitxers de tots els exercicis a la carpeta 'nom-carpeta' 
- [#40] Posar 'make <nivell>' de manera que es generin tots els pdf d'un mateix nivell 

## 0.0.7 

### Notes 

- [#11] Tenir cobert tot el currículum

### Tasks 

- [#10] Mirar el currículum per determinar els tipus d'exercicis automàtics que me falten d'ESPA (tots els nivells) i posar tasques corresponents 
- [#36] Fer una versió web per a què els alumnes s'entrenassin: un alumne podria escollir un exercici i fer-lo. Hauria de ser més modular: definir primer les variables, mostrar a part l'enunciat, les solucions i la resolució 
- [#37] Fer una versió web per a què els alumnes: dur comptabilitat (tipus classroom o deltamath) de qui fa què i una espècie de puntuació 

## 0.1.0 


### Tasks 

- [#44] Refer-ho tot de manera que tengui una interfície per a generar exàmens i deures als alumnes: veure Proposta 0.1 [tasques/proposta-0.1.pdf] 
- [#46] Els exercicis haurien de ser senzills 
- [#47] La proposta-0.1.pdf ha de tenir en compte que necessit BigDecimal per a calcular 0.1+0.2 si ho vull fer bé. Del contrari, no obtendré nombre que toca; Mirar la possibilitat d'implementar-ho amb altres llenguatges (lua pareix que ho fa bé) 
- [#50] exercises 1 4 5 
- [#51] multiplicity 
- [#52] generar pdf usant context a partir del .context generat 
- [#53] Mofificar el Makefile 

