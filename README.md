# Exercicis automàtics de Matemàtiques

**Aquest projecte s'ha abandonat a favor del projecte [worksheet.cc](https://sr.ht/~xavierb/worksheet.cc/)**

## Continguts

Aquí podeu trobar exercicis autogenerats, amb solucions i resolució. La idea és generar exercicis individualitzats per a què els alumnes puguin practicar els exercicis més rutinaris tots sols, o si el professor els vol enviar deures que no es copiïn.

Els exercicis estan distribuïts per continguts. Simplement baixeu el fitxer que vos interessi.

Tots els fitxers es distribueixen sota llicència de [Reconeixement-CompartirIgual 4.0 Internacional de Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.ca) (CC-BY-SA 4.0). L'autoria d'aquesta obra corrrespon a Xavier Bordoy i a eventuals contribucions. El codi es distribueix sota llicència [MIT](https://en.wikipedia.org/wiki/MIT_License). Per a més informació, vegeu el directori `LICENSES/` al codi font.

## Generació d'exercicis

Es generen arxius pdf usant [ConTeXt](https://wiki.contextgarden.net/Main_Page) i l'[ERB](https://ruby-doc.org/stdlib-3.0.2/libdoc/erb/rdoc/ERB.html) (un sistema de plantilles encastat de [ruby](https://www.ruby-lang.org/en/))

Per a generar el fitxer de nom `algunacosa.pdf` heu de seguir els passos següents:

1. `./erbf algunacosa.conTeXt.erb > algunacosa.conTeXt`
1. `context algunacosa.conTeXt`

També hi ha un fitxer [`Makefile`](Makefile) per a generar els arxius `.pdf` automàticament amb `GNU Make`.

## Canvis destacats i coses a fer

Òbviament aquest projecte no està acabat. Si voleu, podeu veure la [llista de coses a fer](tasques/pendents.md) i la [llista de coses fetes](tasques/.taskbook/archive/archive.json). Si voleu, podeu ajudar-me a acabar alguna de les tasques pendents. En tot cas, la vostra obra s'incorporarà al projecte en els mateixos termes de la llicència.

Hi ha un [resum dels canvis més destacats](CANVIS.md).

Si ho desitgeu, podeu donar-me [suport econòmic](https://www.paypal.com/donate/?business=Y47CYDYMBBVX4&no_recurring=0&item_name=creating+new+math+activities%2C+software%2C+exercises+and+reflections&currency_code=EUR).
