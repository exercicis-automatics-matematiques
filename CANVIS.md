# Canvis més destacats del projecte 'Exercicis automàtics de Matemàtiques'

## 0.0.5 [encara no alliberada]

## 0.0.4 [2023-09-30]

- Modularització (creada la biblioteca 'biblioteca.rb')
- Canvi de sintaxi per una de més moderna (versió LMTX de ConTeXt)
- Exercicis de càlcul de paràmetres de dispersió a Estadística descriptiva unidimensional

## 0.0.3.3 [2023-04-09]

- Depuració de codi (tasca #45)
- Correcció de diversos errors amb els paràmetres
- Correcció de diversos errors en la solució o resolució dels exercicis
- Simplificació del fitxer `Makefile` i eliminació dels fitxers `*.conTeXt` (ara només tenc `*.conTeXt.erb` i el `*.pdf`) 
- Resolució de les operacions amb fraccions mitjançant la tècnica de la "carita feliz" i no amb el càlcul d'un denominador comú.
- Embelliment dels exercicis
- Creació d'una interfície web amb sinatra per llistar i mostrar fitxer, i també generar-los (tasca #35)

## 0.0.3.2 [2022-05-08]

- Correcció d'errors a la solució de Proporcionalitat directa

## 0.0.3.1 [2022-03-13]

- Correcció d'errors amb les dimensions dels `Array` de l'ERB

## 0.0.3 [2021-08-07]

- Creat un directori de tasques (amb l'eina taskbook)
- Llevada la secció dels paràmetres de control
- Borrat 'equacions-segon-grau-parentesis' (estava incompleta)
- Resolt error a les 'equacions-primer-grau-parentesis.conTeXt.erb' (tasca #2)
- Simplificació d'entorns
- Resolta expressió que no estava arrodonida a 'equacions-segon-grau-senzilles.conTeXt.erb'
		
## 0.0.2 [2021-07-21]

- Ús de l'ERB estàndard; afegit un script (`/erbf.rb`).
- Copiats tots els entorns de ConTeXt al directori per a què el projecte sigui autocontingut
- Creada la secció de paràmetres de control

## 0.0.1 [2021-07-21]

- Els continguts són bàsicament els del curs 2020-2021
- Afegida l'advertència que les solucions i resolucions estan generades automàticament a tots els documents
- Ús d'Erubis en comptes de l'ERB estàndard (desapareix l'erb de la línia de comandes a la versió 3.0 de ruby)
- Els fitxers .conTeXt.erb generen fitxers pdf amb extensió .pdf i no .conTeXt.pdf
