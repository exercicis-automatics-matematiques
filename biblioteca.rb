# SPDX-FileCopyrightText: 2023 Xavier Bordoy
#
# SPDX-License-Identifier: MIT

# Biblioteca per a tenir diverses funcions amb ruby

module Estadistica
# Mòdul per als exercicis d'Estadística

  ## Retorna la llista de freqüències absolutes de la llista passada com a paràmetre
  def self.taula_frequencies(llista)
	taulafreq = Hash.new

	llista.uniq.each do |valor|
	  taulafreq[valor] = llista.select {|i| i == valor }.length
	end

	return taulafreq.sort.to_h
	# Format {valor => freq, ...}
  end
  
  ## Retorna la llista de freqüències relatives de la llista passada com a paràmetre
  def self.taula_frequencies_relatives(llista)

	# Calculem la taula de freqüències absolutes
	taulafreq = taula_frequencies(llista)
	total = llista.length
	# I ara la taula de freqüències relatives
	taulafreqrel = Hash.new

	taulafreq.each do |valor, freq|
	   taulafreqrel[valor] = (freq/total.to_f).round(3)
	end

	return taulafreqrel
	# Format {valor => freqrel, ...}

  end
  
  ## Retorna la moda de la llista passada com a paràmetre
  def self.moda(llista, cadenaformat=", ")
	# Seleccion les freqüències
	frequencies = taula_frequencies(llista).values
	return frequencies.each_index.select {|index| frequencies[index] == frequencies.max}.map {|i| taula_frequencies(llista).keys[i] }.join(cadenaformat)
	# Format Moda_1 <cadenaformat> Moda_2 <cadenaformat> etc.
  end

  ## Retorna la mediana de la llista passada com a paràmetre
  def self.mediana(llista)
	if llista.length.odd?
		return llista.sort[llista.length/2]
	else
		return ((llista.sort[llista.length/2-1]+llista.sort[llista.length/2])/2.0)
	end
  end
  
  ## Retorna la mitjana aritmètica de la llista passada com a paràmetre
  def self.mitjana(llista)
	return llista.sum.fdiv(llista.size).round(3)
  end
  
  ## Retorna la desviació mitjana de la llista passada com a paràmetre
  def self.desviacio_mitjana(llista)
  	m = mitjana(llista)
    	auxiliar = Array.new()
	llista.each do |p|
	  auxiliar.push((p - m).abs)
	end
	
	return mitjana(auxiliar)
  end

end
