# SPDX-FileCopyrightText: 2024 Xavier Bordoy
#
# SPDX-License-Identifier: GPL-3.0-or-later

- token: "1"
  title: "Equacions de primer grau senzilles"
  category: "equacions-grau-1"
  description: "ax+b=c, a, b, c estrictament positius i menors o iguals que 20"
  pre: |
     a = random.randrange(1, 20)
     b = random.randrange(1, 20)
     c = random.randrange(1, 20)
     solucio = fractions.Fraction(c-b,a)
  text: "Resoleu: ${{a}} x + {{b}} = {{c}}$"
  solution: |
     La solució és $({{c}}-{{b}})/{{a}}$, és a dir, ${{ c-b }}/{{ a }}$, que simplificat és $ {{solucio.numerator}}/{{solucio.denominator }}$.
  resolution: |
     Tenim que ${{a}}x + {{b}} = {{c}}$, per tant, ${{a}}x = {{c}} - {{b}}$ $\Rightarrow$ $x = \frac{ {{c}} -{{b}} }{ {{a}} }$ $\Rightarrow$ $x = \frac{ {{c - b}} }{ {{a}} }$, que simplificat és $x = \frac{ {{solucio.numerator}} }{ {{solucio.denominator}} }$.
- token: "2"
  title: "Equacions de primer grau senzilles"
  category: "equacions-grau-1"
  description: "ax-b=c, a, b, c estrictament positius i menors o iguals que 20"
  pre: |
     a = random.randrange(1, 20)
     b = random.randrange(1, 20)
     c = random.randrange(1, 20)
     solucio = fractions.Fraction(c+b,a)
  text: "Resoleu: ${{a}} x - {{b}} = {{c}}$"
  solution: |
     La solució és $({{c}}+{{b}})/{{a}}$, és a dir, ${{ c+b }}/{{a}}$, que simplificat és ${{solucio.numerator}}/{{solucio.denominator}}$.
  resolution: |
     Tenim que ${{a}}x - {{b}} = {{c}}$, per tant, ${{a}}x = {{c}} + {{b}}$ $\Rightarrow$ $x = \frac{ {{c}} + {{b}} }{ {{a}} }$ $\Rightarrow$ $x = \frac{ {{c + b}} }{ {{a}} }$, que simplificat és $x = \frac{ {{solucio.numerator}} }{ {{solucio.denominator}} }$.
