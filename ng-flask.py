#!/usr/bin/env python

# SPDX-FileCopyrightText: 2024 Xavier Bordoy
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
# Exercicis automàtics

Es tracta d'una biblioteca en python per a generar exercicis automàtics per als alumnes. S'usa fortament [flask](https://flask.palletsprojects.com) i més concretament el sistema de plantilla [Jinja](https://jinja.palletsprojects.com/), creats per n'Armin Ronacher.

El programa es distribueix sota llicència GPL 3 o versió posterior.
"""

import random, fractions
import flask
import yaml

app = flask.Flask(__name__)

# Class Exercise
class Exercise:
    """Implements an automatic exercise"""

    values: dict
    """The dict of values"""

    def __init__(self, values: dict, params: dict={}):
        """Copies `values` to `self.values` and applies `params` to `self.valors` using `flask.render_template_string`"""
        
        self.values = values
        for k, v in values.items():
            self.values[k] = flask.render_template_string(v, **params)

    def __str__(self):
        return "{values}".format(values=self.values)

    def __lt__(self, other):
        """Compares two exercises"""
        return self.title < other.title

    @property
    def title(self) -> str:
        if 'title' in self.values:
            return self.values['title']
        else:
            return ""

    @property
    def solution(self) -> str:
        if 'solution' in self.values:
            return self.values['solution']
        else:
            return ""

    @property
    def text(self) -> str:
        if 'text' in self.values:
            return self.values['text']
        else:
            return ""

    @property
    def resolution(self) -> str:
        if 'resolution' in self.values:
            return self.values['resolution']
        else:
            return ""

# Auxiliar functions
def extract(mylist: list[str], mydiccionari: dict):
    """Extracts Exercises which has `Exercici.token` belonging to `mylist`. `mydiccionari` is a dict from YAML file with `Exercise fields`."""

    exercises = []
    selection = [e for e in mydiccionari if e["token"] in mylist]

    # We run the code of the exercise (in `pre` field)
    for ex in selection:
        _locals = {}

        # Executam el codi de l'exercici i el desem al dict `_locals`
        exec(ex["pre"], globals(), _locals)
        exercise = Exercise(values=ex, params=_locals)
        exercises.append(exercise)

    return exercises


# Flask routes
@app.route("/", methods=["GET"])
def prova():
   with open("ng-exercicis.py", "r") as fitxer:

       # Llegim els continguts del fitxer
       y = yaml.load(fitxer.read(), Loader=yaml.CLoader)

       exercises = extract(["1", "2"], y)
       return flask.render_template("template.tex", exercises=exercises)



@app.route("/list", methods=["GET"])
def list(route="ng-exercicis.py"):
    """Lists all available exercises"""

    with open(route, "r") as f:
        y = yaml.load(f.read(), Loader=yaml.CLoader)

        alls = [e["token"] for e in y]
        exercises = extract(alls, y)
        return flask.render_template("list.html", exercises=exercises)


if __name__ == '__main__':
    app.run(debug=True, port=8080)
