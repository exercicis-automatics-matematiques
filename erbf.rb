#!/usr/bin/env ruby

# SPDX-FileCopyrightText: 2023 Xavier Bordoy
#
# SPDX-License-Identifier: MIT

# Llegeix un fitxer i li aplica l'ERB

require 'erb'

if ARGV.length > 0
  contents = File.read(ARGV[0])
  e = ERB.new(contents).result
  puts(e)
end
