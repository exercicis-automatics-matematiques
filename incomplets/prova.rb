# SPDX-FileCopyrightText: 2023 Xavier Bordoy
#
# SPDX-License-Identifier: MIT

require 'erb'

plantilla = "Calculeu $<%= a %> + <%=b %>$. Resultat: $<%= a + b %>$"


r = ERB.new(plantilla)
a = rand(10)
b = rand(5)

puts r.result()
